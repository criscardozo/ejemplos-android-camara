package code.androidkeeda.camera.com.csameraexample;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

	private Button btnCamera;
	private ImageView capturedImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		btnCamera = (Button) findViewById(R.id.btnCamera);

		capturedImage = (ImageView) findViewById(R.id.capturedImage);

		btnCamera.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openCamera();
			}
		});

		if (savedInstanceState != null) {
			Bitmap bitmap = savedInstanceState.getParcelable("image");
			capturedImage.setImageBitmap(bitmap);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		BitmapDrawable drawable = (BitmapDrawable) capturedImage.getDrawable();
		if (drawable != null) {
			Bitmap bitmap = drawable.getBitmap();
			outState.putParcelable("image", bitmap);
		}
		super.onSaveInstanceState(outState);
	}

	private void openCamera() {
		Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(intent, 0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			Bitmap bp = (Bitmap) data.getExtras().get("data");
			capturedImage.setImageBitmap(bp);
		}
	}

}
